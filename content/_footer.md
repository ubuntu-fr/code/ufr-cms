---
draft: false
headless: true
---
Ubuntu-fr est sur [Libera Chat (IRC)](https://web.libera.chat/#ubuntu-fr), [Telegram](https://t.me/ubuntufrannonce), [Twitter](https://twitter.com/ubuntufrorg), [Facebook](https://www.facebook.com/ubuntufrorg), [Reddit](http://www.reddit.com/r/ubuntufr/) et [LinkedIn](https://www.linkedin.com/groups/47066/).

[Télécharger Ubuntu](/download) - [Ordinateurs vendus avec Ubuntu](https://doc.ubuntu-fr.org/ordinateur_vendu_avec_ubuntu)
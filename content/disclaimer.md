---
title: Mentions Légales
date: 2020-12-21T12:00:55.000Z
draft: false
description: Informations éditoriales, protection et traitement de données à
  caractère personnel
---
# Mentions Légales

## Informations éditoriales

### Éditeur

Association Ubuntu-fr  
Maison des associations  
23 rue Grénéta  
75002 Paris  
SIRET : 81930118500016

### Directeur de publication

Association Ubuntu-fr  

### Hébergeur

Ce site web est hébergé par l'association Ubuntu-fr sur des serveurs dédiés, hébergés par la FSF France et Ielo, ainsi que plusieurs serveurs loués ou mis à dispositions, par Online et OVH.

### Contact

Vous pouvez contacter l'association aux coordonnées ci-dessus ou par courriel&thinsp;:  
**asso@ubuntu-fr.org**

## Protection et traitement de données à caractère personnel

### Recueil et traitement des donnés personnelles

Le site ubuntu-fr.org respecte la vie privée de l’internaute et se conforme strictement aux lois en vigueur sur la protection de la vie privée et des libertés individuelles. Aucune information personnelle n’est collectée à votre insu. Aucune information personnelle n’est cédée à des tiers. Les courriels, les adresses électroniques ou autres informations nominatives dont ce site est destinataire ne font l’objet d’aucune exploitation et ne sont conservés que pour la durée nécessaire à leur traitement.

### Cookies

Les site ubuntu-fr.org n'utilise que les cookies strictement nécessaires à l'utilisation du site. Aucun cookie traceur, qui demanderait le consentement de l'utilisateur, destiné par exemple aux mesures d'audience, publicité, partage sur les réseaux sociaux, etc. n'est déposé.

### Accès à vos données personnelles

Conformément au RGPD et à la Loi Informatique et Libertés, vous bénéficiez d'un droit d'accès, de modification et de suppression de vos données personnelles. Ces données concernent uniquement les informations que vous avez transmis lors de votre inscription sur le site. En cas de demande de suppression totale, le compte sera anonymisé mais les contenus publiés seront conservés.

Toute demande doit être adressée à l'équipe de modération : moderateurs \[at] ubuntu-fr.org

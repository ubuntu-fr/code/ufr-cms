---
title: Rendez-vous Ubuntu-fr
date: 2021-04-21T14:09:48.000Z
draft: false
description: Rendez-vous réguliers de la communauté Ubuntu-fr
---
## Rendez-vous réguliers d'Ubuntu-fr
N'hésitez pas à proposer votre aide et à rejoindre les équipes d'Ubuntu-fr qui tiennent les stands et organisent les Ubuntu Party, par l'intermédiaire du forum événements. Attention, la liste ci-dessous n'est pas exhaustive.
### Webcafé Ubuntu dans les festivals de musique

* [Les Papillons de Nuit](http://www.papillonsdenuit.com/) (Manche), depuis 2011 avec [Festiv'IT](http://festivit.org/).
* [Les Vieilles Charrues](http://www.vieillescharrues.asso.fr/) (Finistère), depuis 2009 avec [Festiv'IT](http://festivit.org/) et [Infothema](http://infothema.fr/) (Historiquement aussi avec [Linux MAO](http://www.linuxmao.org/)).
* [La Route du Rock](http://www.laroutedurock.com/) (Ille-et-Vilaine), en 2014 et 2015 (participation en suspens) avec [Festiv'IT](http://festivit.org/).

### Stand Ubuntu-fr dans les fêtes populaires

* [La Fête de la Récup'](http://www.lafetedelarecup.org/) (Paris), depuis 2015, avec le [REFER](http://www.reemploi-idf.org/).
* La Grande braderie de Lille, depuis 2006 (en suspens depuis 2016), avec [Chtinux](http://chtinux.org/).
* [La Fête de l'Humanité](http://fete.humanite.fr/), depuis 2006 (avec une interruption de 2010 à 2013 et en suspens depuis 2019)

### Salons informatiques

* [Journées du Logiciel Libre](https://jdll.org/) (JDLL), depuis 2009.
* [Rencontres Mondiales du Logiciel Libre](https://rmll.info/) (RMLL), depuis 2005.
* [Open Source Summit](http://opensourcesummit.paris/) (Paris), (remplace Solutions Linux où nous étions depuis 2006).

### Ubuntu Party

Depuis l'existence d'Ubuntu, des événements plus ou moins importants sont organisés à l'occasion de chaque nouvelle version de la distribution.

Dans certaines villes, notamment à [Dijon](http://www.ubuntu-dijon.org/), [Toulouse et Paris](https://ubuntu-paris.org), ces événements à destination du grand-public prennent des proportions extraordinaires, avec par exemple en 2009, 5000 visiteurs en deux jours à Paris.

### Premier Samedi du Libre

Depuis 2008, des membres d'Ubuntu-fr participent [chaque premier samedi du mois à Paris](https://premier-samedi.org), à une après-midi d'activités publiques autour du Libre. Parmi ces activités, une install party multi-distributions, des cours d'initiations et parfois des conférences. 

### Autres événements de la communauté
[Notre forum « événements »](http://forum.ubuntu-fr.org/viewforum.php?id=29) est un bon endroit pour annoncer des rendez-vous autour du libre et d'Ubuntu.

---
title: À Propos
date: 2020-12-21T14:09:48.000Z
draft: false
description: La communauté Ubuntu-fr
---
## La communauté Ubuntu-fr

**C'est vous et toute personne qui utilise Ubuntu.**\
Toute personne que vous croisez au détour du forum ou sur IRC, qui contribue à la documentation, qui anime les stands ou qui anime une Ubuntu Party est membre de la communauté, au même titre que n'importe qui utilisant Ubuntu. La communauté est essentiellement composée de **bénévoles**, qui participent en prenant (parfois beaucoup) de leur temps libre; Ubuntu étant très présent en entreprises, des contributions peuvent également être non-bénévoles.\
**Quels sont les moyens actuels d'ubuntu-fr ?**\
[L'association des francophones utilisant Ubuntu](/association) soutien cette communauté, financièrement et légalement. Elle a pour objet de participer à la diffusion et au développement de la distribution GNU/Linux Ubuntu et des applications qui la composent.

Les membres de la communauté animent et administrent la documentation, les forums, le planet, les canaux IRC, participent à la traduction d'Ubuntu, inventent des goodies et animent les différents événements. Tout ça est l'œuvre de bénévoles, que ce soit de manière ponctuelle ou plus suivie, « anonymement » ou en étant modérateur ou administrateur. Si vous voulez avoir une garantie de réponse, il existe des offres de support payant chez [Canonical](https://canonical.com) et certaines [SSLL](https://fr.wikipedia.org/wiki/Soci%C3%A9t%C3%A9_de_services_en_logiciels_libres).\
**La communauté a besoin d'aide dans différents domaines. Contribuez à ubuntu-fr !**

### Contribuer

Voici différentes façons de contribuer à la communauté ubuntu-fr. Les besoins sont nombreux et très différents. **Tout le monde peut participer.**

* **Promouvoir Ubuntu en portant haut les couleurs d'Ubuntu.**
* Vous pouvez acheter des « goodies » Ubuntu-fr : T-shirt,  mug, clé USB gravée, badges, autocollants… lors de diverses manifestations ubuntu-fr et sur la boutique en ligne EnVenteLibre, qui réunit plusieurs associations de logiciels libres.
* **Animer les stands de l'association** pendant les Ubuntu-Party et autres événements.
* **Participer sur le forum et IRC**
* Répondre aux questions, signaler les messages allant à l'encontre de la charte, etc.
* **Aider à la rédaction de la documentation d'Ubuntu-fr**
* Que vous souhaitiez commencer un texte à partir de rien, corriger des « fôtes daurthograf » ou simplifier des phrases, votre contribution est la bienvenue.
* Si vous vous débrouillez bien en anglais, vous pouvez :

  * **Traduire les documentations et les applications d'Ubuntu et la LHU,**
  * **Participer à l'amélioration d'Ubuntu,**
  * Proposer des idées d'améliorations,
  * Participer directement au développement d'Ubuntu,
  * Trier les bogues existants,
  * Proposer de nouvelles fonctionnalités,
  * Partager vos créations graphiques,
  * Proposer vos idées d'amélioration graphique ou ergonomiques.
* **S'impliquer dans un des projets d'Ubuntu-fr**
* renouveler l'apparence du site, création de nouveaux goodies, etc.

**La plus grande contribution que vous pouvez apporter c'est de partager votre expérience avec votre entourage. Cette contribution, quoique invisible, est la plus importante de toutes.**\
**Ubuntu signifie « Je suis ce que je suis grâce à ce que nous sommes tous », ou simplement Humanité.** Toute aide est la bienvenue, n'hésitez pas à contacter les personnes déjà actives (via les listes de diffusions ou le forum) des différents projets afin de faire vivre la communauté.

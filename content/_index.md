---
title: Accueil
date: 2020-12-21T11:56:01.000Z
draft: false
screenshot: /img/ubuntu_24.04_lts.png
---
{{< photoswipe >}}

Bienvenue sur Ubuntu-fr, la communauté francophone des utilisateurs d'Ubuntu.

## Ubuntu

Ubuntu est un système d'exploitation [GNU/Linux](https://fr.wikipedia.org/wiki/Linux_ou_GNU/Linux) basé sur la distribution [Debian](https://www.debian.org/). Il est [libre](https://fr.wikipedia.org/wiki/Logiciel_libre), gratuit, et simple d'utilisation.

{{< img_gallery src="img/ubuntu_24.04_lts.png" alt="Bureau Ubuntu" sizes="100%" >}}

La première variante d'Ubuntu est fournie avec le bureau [GNOME](https://doc.ubuntu-fr.org/gnome), destiné principalement à un usage bureautique ou domestique. Ubuntu est donc la variante la plus accessible et populaire, et pour laquelle vous trouverez le plus facilement d'aide sur ce site.

Avec Ubuntu vous pouvez facilement et en toute sécurité accomplir tout ce que vous attendez de votre ordinateur : naviguer sur le Web, envoyer et recevoir vos courriels, créer des documents et des présentations, gérer et éditer vos images, musiques et vidéos, jouer, et bien plus encore.
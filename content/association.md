---
title: Association Ubuntu-fr
date: 2021-04-13T14:09:48.000Z
draft: false
description: Association Ubuntu-fr
---
## Ubuntu-fr


L'**association des francophones utilisant Ubuntu**, alias Ubuntu-fr, est une association de loi 1901 déclarée en France depuis 2006. Son existence permet de donner un cadre légal aux activités de la communauté francophone d'Ubuntu, dont ubuntu-fr.org.\
**Ubuntu-fr** a pour objet de participer à la promotion, la diffusion et le développement de la distribution GNU/Linux Ubuntu et des applications qui la composent, en accueillant des personnes utilisatrices ou contributrices d'Ubuntu et en assurant la continuité des travaux collectifs francophones, tels qu'une documentation, un forum et des divers supports de communication sur internet, ainsi que d'événements dédiés, ou de présences à des évènements sur les logiciels libres sur les territoires francophones.\
L'association est à but non lucratif et fonctionne grâce aux dons de la communauté et à la vente des goodies qu'elle produit, disponibles sur les événements et sur la boutique en ligne [EnVenteLibre](https://enventelibre.org/41-ubuntu-fr).\
L'association, en tant que telle, est entièrement composée de bénévoles. Son Conseil d'administration est composé, en 2021, de 8 membres :

* Rudy André, administrateur 
* Rodolphe Bouchier, secrétaire adjoint
* Julie Cotinaud, administratrice
* Olivier Fraysse, trésorier
* Matthieu Joossen, président
* Thomas Joossen, vice-président
* Coralie Landel, secrétaire
* Novakovski Alexandre, administrateur

Pierre Pavard est Délégué régional du Centre-Val de Loire.\
Au 11 avril 2021, l'association compte 30 membres à jour d'adhésion. Pour devenir membre, envoyez votre candidature à **asso at ubuntu-fr ⋅ org** .\
Vous pouvez retrouver les statuts et les différentes informations sur l'association ci-dessous : 

* [Les statuts](https://asso.ubuntu-fr.org/documents/%5B2016-02-21%5D%20Statuts%202016.pdf)
* [Le règlement intérieur](https://asso.ubuntu-fr.org/documents/%5B2017-03-15%5D%20R%C3%A8glement%20int%C3%A9rieur%20et%20Code%20de%20conduite%20Ubuntu-fr.pdf)
* [Compte-rendu de la dernière Assemblée générale (2021)](https://asso.ubuntu-fr.org/documents/%5B2021-04-11%5D%20CR%20-%20Assembl%C3%A9e%20g%C3%A9n%C3%A9rale.pdf)
* [Parution au journal officiel](https://asso.ubuntu-fr.org/documents/%5B2006-08-02%5D%20R%C3%A9c%C3%A9piss%C3%A9%20D%C3%A9claration%20Pr%C3%A9fecture.pdf)

Siège social :  
&emsp;Ubuntu-fr - Maison des associations  
&emsp;23 rue Grénéta  
&emsp;75002 Paris

## Fonctionnement

Les membres sont réunis sur une liste de discussion interne, à partir de laquelle des initiatives peuvent voir le jour et faire émerger des groupes de travail qui peuvent prendre toute forme adaptée aux tâches à accomplir en conformité avec le règlement intérieur.\
Le Conseil d'administration est composé au moins partiellement de membres de la [communauté Ubuntu internationale](https://wiki.ubuntu.com/Membership) et de membres actifs dans les événements organisés par la communauté Ubuntu-fr (sous la responsabilité de l'association); il est en réunion permanente, par le truchement des réseaux de discussion instantanée.\
Les activités de la communauté Ubuntu francophone ne sont pas soumises à l'accord de l'association, qui n'intervient qu'en soutien financier, légal et/ou organisationnel en fonction du besoin.\
Les membres de l'association veillent sur les activités de la communauté dont la responsabilité légale de l'association peut être engagée, tels que les événements, la documentation participative en ligne, les forums, ou tout autre publication sous le domaine ubuntu-fr.org, mais veille plus globalement à ce que la communauté Ubuntu francophone existe et subsiste dans de bonnes dispositions. 

## Financement

Canonical, la société qui sponsorise ubuntu, ne participe d'aucune façon à l'association Ubuntu-fr.
Ubuntu-fr ne touche par ailleurs aucune subvention. Les fonds de l'association proviennent de dons ainsi que de [ventes de produits promotionnels](https://enventelibre.org/41-ubuntu-fr).

## Moyens

L'association s'est équipée pour ses activités événementielles de sensibilisation de 12 ordinateurs et de matériels de communication.\
Au fil des événements, l'association s'est également doté d'un certain nombre d'équipements de communication (oriflammes, bâches, nappes...), de cuisine et de stockage.\
Pour les besoins de ubuntu-fr.org, l'association est propriétaire de trois serveurs dédiés, hébergés par la FSF France, Ielo et Picapo, ainsi que plusieurs serveurs loués ou mis à dispositions, par Online et OVH.\
Un bureau est partagé avec les associations EnVenteLibre et Picapo à Montreuil (93) pour le stockage, pour certains travaux administratifs ou de manutention (préparation d'événéments, remplissage de clés USB, maintenance des postes informatiques..), ainsi que pour quelques réunions (hors périodes de pandémie).

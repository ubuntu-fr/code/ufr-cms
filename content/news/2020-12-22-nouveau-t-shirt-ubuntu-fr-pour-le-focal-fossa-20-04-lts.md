---
title: T-shirt Ubuntu-fr Focal Fossa (20.04 LTS) !
date: 2020-04-22T00:29:00.000Z
description: ""
---

{{< img src="/img/ubuntu-focal-fossa-t-shirt.jpg" alt="T-shirt Groovy Gorilla" href="https://enventelibre.org/fr/ubuntu-fr/171-330-t-shirt-ubuntu-focal-fossa.html" sizes="(max-width: 600px) 100vw, 630px" >}}

**T-shirt Ubuntu-fr pour fêter la version 20.04 (« Focal Fossa ») d'Ubuntu.**

Disponible en quantité limitée sur [enventelibre.org](https://enventelibre.org/fr/ubuntu-fr/171-330-t-shirt-ubuntu-focal-fossa.html) au prix de 20€ au profit de l'association Ubuntu-fr.

Modèle dessiné par [Ocelot](https://www.instagram.com/ohocelot/), bénévole Ubuntu-fr.

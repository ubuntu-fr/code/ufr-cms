---
title: T-shirt Ubuntu-fr Impish Indri (21.10)
date: 2021-10-15T00:25:38.021Z
---

{{< img src="/img/t-shirt-ubuntu-impish-indri.jpg" alt="T-shirt Impish Indri" href="https://enventelibre.org/fr/ubuntu-fr/198-537-t-shirt-ubuntu-impish-indri.html" sizes="(max-width: 600px) 100vw, 630px" >}}

**T-shirt Ubuntu-fr pour fêter la version 21.10 (« Impish Indri ») d'Ubuntu.**
En **pré-commande** sur [enventelibre.org](https://enventelibre.org/fr/ubuntu-fr/190-501-t-shirt-ubuntu-hirsute-hippo.html) au tarif spécial de 15€ jusqu'au 31 octobre 2021, au profit de l'association Ubuntu-fr.

Modèle dessiné par [Ocelot](https://www.instagram.com/ohocelot/), bénévole Ubuntu-fr.

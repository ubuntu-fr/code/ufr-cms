---
title: "9-10 novembre : Ubuntu-fr à l'Open Source Experience"
date: 2021-10-10T00:25:38.021Z
---

{{< img src="/img/logo_osxp_aplat.png" alt="Open Source Experience" href="https://www.opensource-experience.com/" sizes="(max-width: 600px) 100vw, 630px" >}}

**Ubuntu-fr tiendra un stand au salon Open Source Experience, au Palais des congrès à Paris, les 9 et 10 novembre 2021.**
N'hésitez pas à nous rendre visite ou à nous aider à tenir le stand A3 dans le [village associatif](https://www.opensource-experience.com/visiter/village-associatif/)&nbsp;!

---
title: T-shirt Ubuntu-fr Noble Numbat (24.04 LTS)
date: 2024-04-25T00:25:38.021Z
---

{{< img src="/img/t-shirt-ubuntu-noble-numbat.jpg" alt="T-shirt Noble Numbat" href="https://enventelibre.org/fr/ubuntu-fr/220-747-t-shirt-ubuntu-noble-numbat.html" sizes="(max-width: 600px) 100vw, 630px" >}}

**T-shirt Ubuntu-fr pour la version 24.04 (« Noble Numbat ») d'Ubuntu.**

Modèle dessiné par [Ocelot](https://www.instagram.com/ohocelot/).

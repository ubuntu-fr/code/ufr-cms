---
title: T-shirt Ubuntu-fr Hirsute Hippo (21.04)
date: 2021-04-13T00:25:38.021Z
---

{{< img src="/img/ubuntu-hirsute-hippo-t-shirt.jpg" alt="T-shirt Hirsute Hippo" href="https://enventelibre.org/fr/ubuntu-fr/190-501-t-shirt-ubuntu-hirsute-hippo.html" sizes="(max-width: 600px) 100vw, 630px" >}}

**T-shirt Ubuntu-fr pour fêter la version 21.04 (« Hirsute Hippo ») d'Ubuntu.**
En vente sur [enventelibre.org](https://enventelibre.org/fr/ubuntu-fr/190-501-t-shirt-ubuntu-hirsute-hippo.html) au tarif de 20€, au profit de l'association Ubuntu-fr.

Modèle dessiné par [Ocelot](https://www.instagram.com/ohocelot/), bénévole Ubuntu-fr.

---
title: T-shirt Ubuntu-fr Jammy Jellyfish (22.04)
date: 2022-04-15T00:25:38.021Z
---

{{< img src="/img/t-shirt-ubuntu-jammy-jellyfish.jpg" alt="T-shirt Jammy Jellyfish" href="https://enventelibre.org/202-t-shirt-ubuntu-jammy-jellyfish.html" sizes="(max-width: 600px) 100vw, 630px" >}}

**T-shirt Ubuntu-fr pour fêter la version 22.04 (« Jammy Jellyfish ») d'Ubuntu.**

Modèle dessiné par [Ocelot](https://www.instagram.com/ohocelot/), bénévole Ubuntu-fr.

---
title: T-shirt Ubuntu-fr Mantic Minautor (23.10)
date: 2023-10-15T00:25:38.021Z
---

{{< img src="/img/t-shirt-ubuntu-mantic-minotaur.jpg" alt="T-shirt Mantic Minautor" href="https://enventelibre.org/fr/ubuntu-fr/219-734-t-shirt-ubuntu-mantic-minotaur.html" sizes="(max-width: 600px) 100vw, 630px" >}}

**T-shirt Ubuntu-fr pour la version 23.10 (« Mantic Minotaur ») d'Ubuntu.**

Modèle dessiné par [Ocelot](https://www.instagram.com/ohocelot/).

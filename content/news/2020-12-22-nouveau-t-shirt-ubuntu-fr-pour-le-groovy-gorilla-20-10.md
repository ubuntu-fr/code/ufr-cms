---
title: T-shirt Ubuntu-fr Groovy Gorilla (20.10)
date: 2020-10-22T00:25:38.021Z
---

{{< img src="/img/ubuntu-groovy-gorilla-t-shirt.jpg" alt="T-shirt Groovy Gorilla" href="https://enventelibre.org/fr/ubuntu-fr/179-t-shirt-ubuntu-groovy-gorilla.html" sizes="(max-width: 600px) 100vw, 630px" >}}

**T-shirt Ubuntu-fr pour fêter la version 20.10 (« Groovy Gorilla ») d'Ubuntu.**

Disponible en quantité limitée sur [enventelibre.org](https://enventelibre.org/fr/ubuntu-fr/179-t-shirt-ubuntu-groovy-gorilla.html) au prix de 20€ au profit de l'association Ubuntu-fr.

Modèle dessiné par [Ocelot](https://www.instagram.com/ohocelot/), bénévole Ubuntu-fr.

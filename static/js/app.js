import { getCookie, setCookie } from './cookie.js'

let cookieTheme = getCookie('theme')
let cookieWidth = getCookie('width')
let cookieMaxAge = 365*24*60*60

let domain = window.location.hostname
let sld = domain.replace(/^[^.]+\./g, "")

function changeTheme(theme) {
  if (theme == 'light') {
    document.querySelector('#switcher-light').style.display = 'none'
    document.querySelector('#switcher-dark').style.display = 'block'
    document.documentElement.classList.remove('dark')
    if (sld == 'localhost') setCookie('theme','light',{maxAge:cookieMaxAge,path:'/'})
    else setCookie('theme','light',{domain:sld,maxAge:cookieMaxAge,path:'/'})
  }
  else {
    document.querySelector('#switcher-dark').style.display = 'none'
    document.querySelector('#switcher-light').style.display = 'block'
    document.documentElement.classList.add('dark')
    if (sld == 'localhost') setCookie('theme','dark',{maxAge:cookieMaxAge,path:'/'})
    else setCookie('theme','dark',{domain:sld,maxAge:cookieMaxAge,path:'/'})
  }
}
if (cookieTheme) changeTheme(cookieTheme)
else changeTheme('light')

function changeWidth() {
  document.documentElement.classList.toggle('wide')
  if (document.documentElement.classList.contains('wide')) {
    if (sld == 'localhost') setCookie('width','wide',{maxAge:cookieMaxAge,path:'/'})
    else setCookie('width','wide',{domain:sld,maxAge:cookieMaxAge,path:'/'})
  }
  else {
    if (sld == 'localhost') setCookie('width','narrow',{maxAge:cookieMaxAge,path:'/'})
    else setCookie('width','narrow',{domain:sld,maxAge:cookieMaxAge,path:'/'})
  }
}
function instantWiden() {
  document.querySelector('body').style.transition = 'none'
  document.documentElement.classList.add('wide')
  setTimeout( () => {
    document.querySelector('body').style.transition = 'width .8s'
  }, 1)
}
if (cookieWidth == 'wide') instantWiden()

function hideMenu() {
  document.querySelector('body > header').classList.remove('opened')
}

document.addEventListener('DOMContentLoaded', () => {
  document.querySelector('#switcher-light').addEventListener('click', () => {
    changeTheme('light')
  })
  document.querySelector('#switcher-dark').addEventListener('click', () => {
    changeTheme('dark')
  })
  document.querySelector('#sandwich').addEventListener('click', () => {
    document.querySelector('body > header').classList.toggle('opened')
  })
  document.querySelectorAll('header .switcher, header nav a').forEach( (item) => {
    item.addEventListener('click', () => {
      hideMenu()
    })
  })
  document.querySelector('#switcher-wide').addEventListener('click', () => {
    changeWidth()
  })
})

let variante = 'ubuntu'
let lts
let interim

const variantes = {
    edubuntu: 'Edubuntu',
    kubuntu: 'Kubuntu',
    lubuntu: 'Lubuntu',
    ubuntu: 'Ubuntu',
    'ubuntu-budgie': 'Ubuntu Budgie',
    ubuntucinnamon: 'Ubuntu Cinnamon',
    'ubuntu-mate': 'Ubuntu Mate',
    ubuntustudio: 'Ubuntu Studio',
    'ubuntu-unity': 'Ubuntu Unity',
    xubuntu: 'Xubuntu'
}

function humanFileSize(bytes, si = true, dp = 1) {
    const thresh = si ? 1000 : 1024

    if (Math.abs(bytes) < thresh) {
        return bytes + ' B'
    }

    const units = si
        ? ['ko', 'Mo', 'Go', 'To', 'Po', 'Eo', 'Zo', 'Yo']
        : ['Kio', 'Mio', 'Gio', 'Tio', 'Pio', 'Eio', 'Zio', 'Yio']
    let u = -1
    const r = 10 ** dp

    do {
        bytes /= thresh
        ++u
    } while (Math.round(Math.abs(bytes) * r) / r >= thresh && u < units.length - 1)

    return bytes.toFixed(dp) + ' ' + units[u]
}

const getData = async () => {
    const eolData = await fetch(`https://endoflife.date/api/ubuntu.json`)
    const eolJson = await eolData.json()
    const today = new Date()
    for (let version of eolJson) {
        let release = new Date(version.releaseDate)
        if (release.getTime() < today.getTime()) {
            if (!interim) {
                interim = version
            }
            //if (version.cycle == "24.04") version.lts = true // TEMPORAIRE contournement bug https://github.com/endoflife-date/endoflife.date/pull/5046#issuecomment-2079276263
            //if (version.latest == "24.04") version.latest = "24.04.1" // TEMPORAIRE : https://github.com/endoflife-date/endoflife.date/issues/5766
            //if (version.cycle == "24.10") version.latest = "24.10" // TEMPORAIRE : https://github.com/endoflife-date/endoflife.date/issues/6009
            if (version.lts) {
                lts = version
                break
            }
        }
    }
    //console.log('LTS :', JSON.stringify(lts), '\ninterim :', JSON.stringify(interim))
    document.getElementById('links').classList.remove('spinner') // à commenter pour le plaisir...
    makeLinks()
}

const getSize = async (url) => {
    //const response = await fetch('https://corsproxy.io/?'+url, { // en attendant de trouver mieux (proxy CORS auto-hébergé ?)
    const response = await fetch(url, {
        method: 'HEAD',
        redirect: "follow"
    })
    const fileSize = response.headers.get('Content-Length')
    return humanFileSize(fileSize)
}
function showSizes() {
    document.querySelectorAll('#links a.button').forEach((link) => {
        getSize(link.href).then((size) => {
            link.title = size
        }).catch((error) => {
            console.error('Error :', error)
        })
    })
}

function makeLink(distro, recommend = false) {
    const eol = new Date(distro.eol)
    let date = eol.toLocaleDateString("fr", { month: 'long' }) + ' ' + eol.getFullYear()
    let type
    let recommended = ''
    if (recommend) recommended = ' (recommandée)'
    if (distro.lts) {
        type = 'LTS'
        date = eol.toLocaleDateString("fr", { month: 'long' }) + ' ' + (eol.getFullYear())
        /*if (variante != "ubuntu") { // 3 ans de support au lieu de 5 pour les variantes
            date = eol.toLocaleDateString("fr", { month: 'long' }) + ' ' + (eol.getFullYear() - 2)
        }*/
    }
    else {
        type = 'intermédiaire'
    }

    let url
    let cheksum

    if (variante == "ubuntu") {
        url = `https://releases.ubuntu.com/${distro.latest}/ubuntu-${distro.latest}-desktop-amd64.iso`
        cheksum = `https://releases.ubuntu.com/${distro.latest}/SHA256SUMS`
    }
    else if (variante == "ubuntustudio") { // besoin d'ajouter dvd dans l'url
        url = `https://cdimage.ubuntu.com/${variante}/releases/${distro.latest}/release/${variante}-${distro.latest}-dvd-amd64.iso`
        cheksum = `https://cdimage.ubuntu.com/${variante}/releases/${distro.latest}/release/SHA256SUMS`
    }
    else {
        url = `https://cdimage.ubuntu.com/${variante}/releases/${distro.latest}/release/${variante}-${distro.latest}-desktop-amd64.iso`
        cheksum = `https://cdimage.ubuntu.com/${variante}/releases/${distro.latest}/release/SHA256SUMS`
    }

    let title = document.createElement('h3')
    let titleText = document.createTextNode(variantes[variante] + ' ' + distro.codename + ' ' + distro.latest)
    title.appendChild(titleText)

    let description = document.createElement('p')
    let descriptionText = document.createTextNode(`version ${type} maintenue jusqu\'en ${date}${recommended}`)
    description.appendChild(descriptionText)

    let button = document.createElement('a')
    button.classList.add('button')
    let downloadText = document.createTextNode('Télécharger')
    button.href = url
    button.appendChild(downloadText)

    let alternate = document.createElement('p')
    alternate.innerHTML = `<a href="${url}.torrent">torrent</a> | <a href="${cheksum}" target="_blank">somme de contrôle</a>`

    let link = document.createElement('div')
    link.classList.add('download')
    link.appendChild(title)
    link.appendChild(description)
    link.appendChild(button)
    link.appendChild(alternate)

    return link
}

function makeLinks() {
    document.getElementById('links').innerHTML = ''

    if (lts == interim) link = makeLink(lts)
    else link = makeLink(lts, true)
    document.getElementById('links').appendChild(link)

    if (lts != interim) {
        link = makeLink(interim)
        document.getElementById('links').appendChild(link)
    }
    showSizes()
}

document.addEventListener('DOMContentLoaded', () => {
    document.querySelector('#links').classList.add('spinner')
    let url = new URL(location) // on récupère le nom de la distro dans l'URL
    let linked = url.pathname.split('/')[2]
    for (const [codeName, fullName] of Object.entries(variantes)) {
        let option = document.createElement('option')
        let optionText = document.createTextNode(fullName)
        option.value = codeName
        option.appendChild(optionText)
        if (linked && variantes[linked]) {
            variante = linked
            if (codeName == linked) {
                option.selected = true
            }
        }
        else {
            if (codeName == 'ubuntu') {
                option.selected = true
            }
        }
        document.getElementById('variante').appendChild(option)
    }
    document.querySelector('#variante').addEventListener('change', (event) => {
        variante = event.target.value
        history.replaceState({}, "test", "/download/" + variante)
        makeLinks()
    })
    getData().catch((error) => {
        console.error('Error :', error)
        document.getElementById('links').classList.remove('spinner')
        document.getElementsByTagName("noscript")[0].outerHTML = document.getElementsByTagName("noscript")[0].innerHTML
        document.getElementById('message').outerHTML = '<p>Excusez-nous !</p>\
            <p>\
                Cette page a rencontré l\'erreur suivante en essayant d\'obtenir des informations depuis le service\
                <a href="https://endoflife.date/api/ubuntu.json">endoflife.date</a> :\
            </p>\
            <pre>' + error + '</pre>\
            <p>\
                N\'hésitez pas à remonter le problème sur\
                <a href="https://forum.ubuntu-fr.org/viewforum.php?id=21">le forum</a>\
                ou sur <a href="https://gitlab.com/ubuntu-fr/code/ufr-cms/-/issues">GitLab</a>.\
            </p>'
    })
})

# Pile [Jamstack](https://jamstatic.fr/2019/02/07/c-est-quoi-la-jamstack/) pour la gestion du contenu éditorial du site ubuntu-fr.org

basé sur [Hugo](https://gohugo.io/), [Decap CMS](https://decapcms.org) et [GitLab](https://gitlab.com).

## utilisation

- front-end : https://www.ubuntu-fr.org/
- gestion du contenu : https://www.ubuntu-fr.org/admin/

Les modifications et publications de contenus sont en fait des commits git visibles [ici](https://gitlab.com/ubuntu-fr/code/ufr-cms/-/commits/main). Tout est versionné !

## gestion des accès

L'accès à la gestion du contenu se fait au moyen d'un compte [GitLab](https://gitlab.com/users/sign_in).  
L'utilisateur doit avoir au minimum un rôle _Maintainer_ sur le projet [ufr-cms](https://gitlab.com/ubuntu-fr/code/ufr-cms/-/project_members) ou le groupe [ubuntu-fr/code](https://gitlab.com/groups/ubuntu-fr/code/-/group_members).

## développement

### Installer Docker et Docker Compose

- [Docker](https://docs.docker.com/install/#supported-platforms)
- [Docker Compose](https://docs.docker.com/compose/install/#install-as-a-container)

(sur Ubuntu il suffit d'installer les paquets [docker.io](apt://docker.io) et [docker-compose](apt://docker-compose))

### Cloner le dépôt de sources

```sh
git clone git@gitlab.com:ubuntu-fr/code/ufr-cms.git --recurse-submodules
```

### Démarrer l'application

```sh
cd ufr-cms
make start
```

### C'est prêt

- front-end : http://ufr-cms.localhost/
- l'interface de gestion du contenu n'est pas accessible localement
- liste des commandes disponibles : `make help`
